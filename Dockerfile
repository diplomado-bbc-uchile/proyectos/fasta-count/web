FROM node:18 AS install

WORKDIR /app

COPY . .

RUN yarn install

CMD yarn start
