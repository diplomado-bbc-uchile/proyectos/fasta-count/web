# Frontend

- [Frontend](#frontend)
  - [Requisitos](#requisitos)
  - [Ejecución](#ejecución)

## Requisitos

Instalar los requisitos del sistema
```
apk update; apk add yarn;
```

Clonar el repositorio y entrar en la carpeta
```
git clone https://gitlab.com/diplomado-bbc-uchile/proyectos/fasta-count/web.git;
cd web;
```

Instalar las dependencias
```
yarn install;
```

Definir las variables de entorno de conexión con la API
```
export REACT_APP_API_URL=http://localhost:5000
```

## Ejecución

Ejecutar
```
yarn start
```

Si todo sale bien, se debería poder acceder a la url `http://direccion-ip:3000`.