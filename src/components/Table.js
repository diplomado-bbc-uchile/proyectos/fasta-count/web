import React from 'react';

const Table = ({ fastas }) => {
  return (
    <table className="table container">
      <thead>
        <tr>
          <th>Filename</th>
          <th>Sequences</th>
          <th>Hash (sha1)</th>
          <th>Date uploaded</th>
        </tr>
      </thead>
      <tbody>
        { (fastas.length > 0) ? fastas.map( (file, index) => {
           return (
            <tr key={ index }>
              <td><a href={ `${process.env.REACT_APP_API_URL}/fasta/${file.filename}` }>{ file.filename }</a></td>
              <td>{ file.sequences }</td>
              <td>{ file.sha1}</td>
              <td>{ file.date_created}</td>
            </tr>
          )
         }) : <tr><td colSpan="5">0 files.</td></tr> }
      </tbody>
    </table>
  );
}

export default Table