import React, { Component } from "react";
import architecture from '../architecture.png'; // with import
 
class Home extends Component {
  render() {
    return (
      <div className="container">
        <h2>Heloo!!</h2>
        <p>This is a micro tool created to demonstrate how interaction is in an application with several layers.</p>
 
        <p>Its main function is to load a fasta file and count the number of sequences. But, to demonstrate the operation in levels, we divided the functionalities into 3 layers.</p>

        <p><b>Frontend:</b> This interface that you see</p>
        <p><b>Micro API:</b> Used to upload, list and download files</p>
        <p><b>Database:</b> Registers files that have already been uploaded</p>

        <p><img className="img-fluid" src={architecture} alt="architecture"/>
     </p>
      </div>
    );
  }
}
 
export default Home;