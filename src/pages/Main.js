import React, { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../index.css';
import {
    Route,
    NavLink,
    HashRouter
  } from "react-router-dom";
  import Home from "./Home";
  import Report from "./Report";
  import Upload from "./Upload";
require('dotenv').config()

class Main extends Component {
  render() {
    return (
      <HashRouter>
        <div>
          <h1>FSC - Fasta Sequence Count</h1>
          <ul className="header">
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/upload">Upload</NavLink></li>
            <li><NavLink to="/report">Report</NavLink></li>
          </ul>
          <div className="content">
            <div className="content">
              <Route exact path="/" component={Home}/>
              <Route path="/upload" component={Upload}/>
              <Route path="/report" component={Report}/>
            </div>
          </div>
        </div>
      </HashRouter>
    );
  }
}
 
export default Main;