import React, { Component } from 'react';
import Table from '../components/Table.js';

class Report extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fastas: []
    }
  }

  componentDidMount() {
    fetch(`${process.env.REACT_APP_API_URL}/fasta/list`)
    .then(res => res.json())
    .then(json => json.files)
    .then(fastas => this.setState({ 'fastas': fastas }))
  }

  render() {
    return (
        <Table fastas={ this.state.fastas } />
    );
  }
}

export default Report;
