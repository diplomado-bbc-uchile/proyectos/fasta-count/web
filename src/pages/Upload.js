import React from 'react';

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMessage: false,
      message: null
    }

    this.handleUpload = this.handleUpload.bind(this);
  }

  handleUpload(ev) {
    ev.preventDefault();

    const data = new FormData();
    data.append('file', this.uploadInput.files[0]);

    fetch(`${process.env.REACT_APP_API_URL}/fasta/upload`, {
      method: 'POST',
      body: data,
    }).then((response) => {
      response.json().then((body) => {
        console.info(body);
        if (body.error) {
          this.setState({ 'showMessage': true, 'message': body.error})
        } else {
          this.setState({ 'showMessage': true, 'message': "Upload successful!"})
        }
      });
    });
  }

  render() {
    return (
      <div className="container">
        <h1>File Upload</h1>
        <form onSubmit={this.handleUpload}>
          <div>
            <input ref={(ref) => { this.uploadInput = ref; }} type="file" />
          </div>
          <br />
          <div>
            <button>Upload</button>
          {this.state.showMessage &&
            <p><label><b>{this.state.message}</b></label></p>
          }
          </div>
          <div>
            <p><label><b><a href="/sample.fasta">Sample.fasta</a> file example</b></label></p>
          </div>
        </form>
      </div>
    );
  }
}

export default Main;
